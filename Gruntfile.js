'use strict';   // Native JS

module.exports = function(grunt) {  // this is how we define Node modules.

    /*
        Grunt Plugins (Node Modules):
    */
    require('time-grunt')(grunt);   // 

    require('jit-grunt')(grunt, {   // loads in other Node modules/Grunt Plugins, when they are actually implied
        useminPrepare: 'grunt-usemin'   // have to explicity specify which plugin to use, otherwise it will search around
    });    

    grunt.initConfig({
        sass: {
            dist: {
                files: {
                    'css/styles.css': 'css/styles.scss'
                }
            }
        },
        watch: {
            files: 'css/*.scss', 
            tasks: ['sass']
        },
        browserSync: {
            dev: {
                bsFiles: {
                    src: [
                        'css/*.css',
                        '*.html',
                        'js/*.js'
                    ]
                },
                options: {
                    watchTask: true,
                    server: {
                        baseDir: './'
                    }
                }
            }
        },
        copy: {
            html: {
                files: [{
                    expand: true,
                    dot: true,
                    cwd: './',
                    src: ['*.html'],
                    dest: 'dist'
                }]
            },
            fonts: {
                files: [{
                    expand: true,
                    dot: true,
                    cwd: 'node_modules/font-awesome',
                    src: ['fonts/*.*'],
                    dest: 'dist'
                }]
            }
        },
        clean: {
            build: {
                src: ['dist/']
            }
        },
        imagemin: {
            dynamic: {
                files: [{
                    expand: true,
                    dot: true,
                    cwd: './',
                    src: ['img/*.{png,gif,jpg}'],
                    dest: 'dist/'
                }]
            }
        },
        useminPrepare: {
            foo: {
                dest: 'dist',
                src: ['contactus.html', 'aboutus.html', 'index.html']
            },
            options: {
                flow: {
                    steps: {
                        css: ['cssmin'],
                        js: ['uglify']
                    },
                    post: {
                        css: [{
                            name: 'cssmin',
                            // this fixes problems with font-awesome using Grunt
                            createConfig: function(context, block) {
                                var generated = context.options.generated;
                                generated.options = {
                                    keepSpecialComments: 0, rebase: false
                                };
                            }
                        }]
                    }
                }
            }
        },  
        concat: { // configured my usemine prepare earlier
            options: {
                separator: ';'
            },
            dist: {}
        },  // the following need to be specified explicitly otherwise usemin won't work properly
        uglify: {
            dist: {}
        },
        cssmin: {
            dist: {}
        },
        filerev: {  /* when usemin prepares the mian js file, filerev adds an additionly extension 
            to the main so that when you prepare a new version and update to site, if someone browser 
            has cached the old page, when the browser dl's the new version, it may not dl all the files 
            because certain files will be found already in the cache. this adds a file revison # to the 
            file names so the browser always recognizes the new files.
                    */
            options: {
                encoding: 'utf8',
                algorithm: 'md5',
                length: 20
           },
           release: {
               files: [{
                   src: [
                       'dist/js/*.js',
                       'dist/css/*.css'
                   ]
               }]
           }
        },
        usemin: {
            html: ['dist/contactus.html', 'dist/aboutus.html', 'dist/index.html'], // specify which files it needs to change/update
            options: {
                assetsDirs: ['dist', 'dist/css', 'dist/js']
            }
        },
        htmlmin: {  // has to be applied after usemin
            dist: {
                options: {
                    collapseWhitespace: true
                },
                files: {
                    'dist/index.html': 'dist/index.html',
                    'dist/aboutus.html': 'dist/aboutus.html',
                    'dist/contactus.html': 'dist/contactus.html'
                }
            }
        }
    });

    grunt.registerTask('css',['sass']); // execute the sass task
    grunt.registerTask('default',['browserSync','watch']);
    grunt.registerTask('build',[
        'clean',
        'copy',
        'imagemin',
        'useminPrepare',
        'concat',
        'cssmin',
        'uglify',
        'filerev',
        'usemin',
        'htmlmin'
    ]);
};